import React from "react";
import "../css/calls.css";
import Call from "./Call";

// Display only archive calls
function ArchiveCalls({ archiveCalls, handleArchiveCall }) {
  return archiveCalls.map((call) => (
    <Call key={call.id} call={call} handleArchiveCall={handleArchiveCall} showArchive />
  ));
}

export default ArchiveCalls;

import React, { useState } from "react";
import moment from "moment";
import archive from "../Assets/Icons/archive-64.png";
import unarchive from "../Assets/Icons/unarchive.png";
import missedCall from "../Assets/Icons/missed-call-24.png";
import incomingCall from "../Assets/Icons/incoming-call-24.png";
import voiceMail from "../Assets/Icons/voicemail-24.png";

const getIcon = (action) => {
  switch (action) {
    case "missed":
      return missedCall;
    case "answered":
      return incomingCall;
    case "voicemail":
      return voiceMail;
    default:
      return "";
  }
};

// Common component for call details
function Call({ call, handleArchiveCall, showArchive = false }) {
  const [callDetailsShow, setCallDetailsShow] = useState(false);

  return (
    call.is_archived === showArchive && (
      <React.Fragment key={`call-${call.id}`}>
        <div className="call-date">
          <span className="date-label">{moment(call.created_at).format("MMMM, Do YYYY")}</span>
        </div>

        <div className="call-log" onClick={() => setCallDetailsShow(!callDetailsShow)}>
          <div className="call-log-inner">
            <img src={getIcon(call.call_type)} alt="call-status" />
            <div className="flex-grow-1">
              <p className="call-number">
                <strong>{call.to}</strong>
              </p>
              <span className="call-via-text">Call via {call.via}</span>
            </div>
            <div className="call-time">{moment(call.created_at).format("LT")}</div>
          </div>

          <div className={callDetailsShow ? "call-details active" : "call-details"}>
            <div className="call-details-inner">
              <lable>{call.from}</lable>
              <lable>{call.duration}s </lable>
              <img
                onClick={() => handleArchiveCall(call.id, !showArchive)}
                src={showArchive ? unarchive : archive}
                alt="archive-call"
                className="archive-btn"
              />
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  );
}

export default Call;

import React, { useEffect, useState } from "react";
import { get, post } from "../Helper/service";
import ArchiveCalls from "./ArchiveCalls";
import Call from "./Call";

function Body() {
  const [showArchive, setShowArchive] = useState(false);
  const [calls, setCalls] = useState([]);

  const getInitialData = async () => {
    const allCallsData = await get("activities");
    setCalls(allCallsData);
  };

  useEffect(() => {
    getInitialData();
  }, []);

  // Archive call handler
  const handleArchiveCall = async (callId, isArchive) => {
    const resBody = await post(`activities/${callId}`, { is_archived: isArchive });

    if (resBody) {
      calls.map((call) => {
        if (call.id === callId) call.is_archived = resBody.is_archived;
      });
      setCalls([...calls]);
    }
  };

  // Reset will unarchive all calls
  const resetCalls = async () => {
    const resBody = await get("reset");
    if (resBody && resBody.message === "done") {
      getInitialData();
    }
  };

  return (
    <>
      <div className="top-menu">
        <button onClick={() => setShowArchive(false)}>Activity Feed</button>
        <button onClick={() => setShowArchive(true)}>Archive Calls</button>
        <button onClick={() => resetCalls()}>Reset</button>
      </div>

      {!showArchive &&
        calls.length > 0 &&
        calls.map((call) => (
          <Call key={call.id} call={call} handleArchiveCall={handleArchiveCall} />
        ))}

      {showArchive && <ArchiveCalls handleArchiveCall={handleArchiveCall} archiveCalls={calls} />}
    </>
  );
}

export default Body;

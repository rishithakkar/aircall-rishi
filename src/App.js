import "./css/app.css";
import "./css/header.css";
import Header from "./Components/Header";
import Body from "./Components/Body";

function App() {
  return (
    <div className="container">
      <Header />
      <div className="container-view">
        <Body />
      </div>
    </div>
  );
}

export default App;
